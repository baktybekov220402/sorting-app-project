package com.baktybekov;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingUtilSortTest extends TestCase {
    private int[] data;
    private int[] expected;

    public SortingUtilSortTest(int[] data, int[] expected) {
        this.data = data;
        this.expected = expected;
    }

    @Test
    public void test() {
        SortingUtil.sort(data);
        assertArrayEquals(data, expected);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testSortingData() {
        return Arrays.asList(new Object[][] {
                {new int[]{1, 2, 3, 4},new int[]{1, 2, 3, 4},},
                {new int[]{4, 3, 2, 1},new int[]{1, 2, 3, 4},},
                {new int[]{6, 2, 4, 0, 2},new int[]{0, 2, 2, 4, 6},},
        });
    }
}