package com.baktybekov;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class StringToIntArrayParserIllegalArgumentsTest extends TestCase {
    private final String[] data;

    public StringToIntArrayParserIllegalArgumentsTest(String[] data) {
        this.data = data;
    }

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        StringToIntArrayParser.parse(data);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"1.5", "2", "3", "4"}},
                {new String[]{"1", "good morning", "-3", "4"}},
        });
    }
}