package com.baktybekov;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ArrayValidatorValidArgumentTest extends TestCase {

    private final int[] data;
    public ArrayValidatorValidArgumentTest(int[] data) {
        this.data = data;
    }

    @Test
    public void test() {
        ArrayValidator.createSizeOfArrayBetween1To10Validator().validate(data);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new int[]{1, 2, 3, 4, 5}},
                {new int[]{0}},
        });
    }
}