package com.baktybekov;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class StringToIntArrayParserParseTest extends TestCase {
    private final String[] data;
    private final int[] expected;

    public StringToIntArrayParserParseTest(String[] data, int[] expected) {
        this.data = data;
        this.expected = expected;
    }

    @Test
    public void test() {
        int[] actual = StringToIntArrayParser.parse(data);
        assertArrayEquals(actual, expected);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"1", "2", "3", "4"}, new int[]{1, 2, 3, 4}},
                {new String[]{"1", "2", "-3", "4"}, new int[]{1, 2, -3, 4}},
                {new String[]{}, new int[]{}},
                {new String[]{"123", "-753463"}, new int[]{123, -753463}}
        });
    }
}