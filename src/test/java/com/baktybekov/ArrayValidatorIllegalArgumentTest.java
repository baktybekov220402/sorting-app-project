package com.baktybekov;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ArrayValidatorIllegalArgumentTest extends TestCase {

    private final int[] data;
    public ArrayValidatorIllegalArgumentTest(int[] data) {
        this.data = data;
    }

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        ArrayValidator.createSizeOfArrayBetween1To10Validator().validate(data);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new int[]{}},
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}},
        });
    }
}