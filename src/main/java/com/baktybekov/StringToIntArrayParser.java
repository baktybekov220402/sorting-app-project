package com.baktybekov;

public class StringToIntArrayParser {
    public static int[] parse(String[] args) throws IllegalArgumentException {
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException(args[i] + " is not an integer format");
            }
        }
        return numbers;
    }
}