package com.baktybekov;

import java.util.function.Predicate;

public class ArrayValidator {
    private final Predicate<int[]> predicate;
    private ArrayValidator(Predicate<int[]> predicate) {
        this.predicate = predicate;
    }

    public static ArrayValidator createSizeOfArrayBetween1To10Validator() {
        return createCustomValidator(ints -> ints.length > 0 && ints.length <= 10);
    }
    public static ArrayValidator createCustomValidator(Predicate<int[]> predicate) {
        return new ArrayValidator(predicate);
    }
    public void validate(int[] array) {
        if(!predicate.test(array)) {
            throw new IllegalArgumentException("Array is not valid");
        }
    }
}
