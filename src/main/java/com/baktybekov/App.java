package com.baktybekov;
import java.util.Arrays;
import static com.baktybekov.ArrayValidator.*;

public class App {
    public static void main(String[] args) {
        int[] intArgs = StringToIntArrayParser.parse(args);
        createSizeOfArrayBetween1To10Validator().validate(intArgs);
        SortingUtil.sort(intArgs);
        System.out.println(Arrays.toString(intArgs));
    }
}